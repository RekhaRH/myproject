package com.example.workflow.tasks;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;
@Component
public class ServiceAgePrint implements JavaDelegate {

	@Override
	public void execute(DelegateExecution execution) throws Exception {
	
		System.out.println("output"+execution.getVariable("age"));

	}

}
